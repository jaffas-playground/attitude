import QtQuick 1.0
import com.meego 1.0

//PageStackWindow {
Main {
    id: window
    anchors.fill: parent
/*    showStatusBar: true
#    showToolBar: false

#    initialPage: Page {
#        orientationLock: PageOrientation.LockLandscape
        Main {}
    }
*/
    Component.onCompleted: {
        if (platformWindow)
            activeMonitor.external = true
        console.log("Set activeMonitor external modification = " + activeMonitor.external)
    }

    Connections {
        target: platformWindow

        onVisibleChanged: updateVisible()
    }


    function updateVisible() {
        console.log("Visible = " + platformWindow.visible)
        activeMonitor.active = platformWindow.visible
    }
}
