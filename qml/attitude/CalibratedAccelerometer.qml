import QtQuick 1.0
import QtMobility.sensors 1.1

import "priv.js" as P

/**
 * The accelerometer control logic.
 *
 * The following properties are exposed for external use:
 *   * dip (double). -1.0 - 1.0 representing tilt of device.
 *   * angle (double). -180 - 180 representing rotation of device.
 *
 * The following method is available:
 *   * calibrate() - consider the current values as centred.
 */
Accelerometer {
    id: self

    property double dip: 0.0
    property double angle: 0.0
    property variant calibration: [0.0, 0.0]

    property int samples: 10

    property bool windowActive: activeMonitor.active

    signal movement(double dip, double angle)

    dataRate: 25 // Harmattan defaults to 0

    /**
     * Track visibility.
     */
    onWindowActiveChanged: {
        console.log("WindowActiveChanged = " + activeMonitor.active)
        if (activeMonitor.active) {
            start();
        } else {
            stop();
        }
    }


    /**
     * Consider the current position as our default centre.
     */
    function calibrate() {
        if (calibration[0] == 0.0 || calibration[1] == 0.0) {
            calibration = [dip, angle]
        } else {
            calibration = [0.0, 0.0]
        }
    }


    /**
     * Start receiving values immediately.
     */
    Component.onCompleted: {
        start();
        console.log("Portrait = " + portraitDevice)
        P.priv(self).previousDip   = []
        P.priv(self).previousAngle = []
    }


    /**
     * When the reading changes, filter and then expose.
     */
    onReadingChanged: {
        var rawDip = Math.max(Math.min(reading.z / 9.8, 1), -1)
        var rawAngle = (portraitDevice ? Math.atan2(-reading.y, reading.x) : Math.atan2(reading.x, reading.y)) * 180/Math.PI

        var newDip   = _updateList(P.priv(self).previousDip, rawDip, 1, 1000) - calibration[0]
        var newAngle = _updateList(P.priv(self).previousAngle, rawAngle, 360.0, 100.0) - calibration[1]
        if (newDip != self.dip || newAngle != self.angle) {
            self.dip   = newDip
            self.angle = newAngle
            self.movement(newDip, newAngle)
        }
    }


    /**
     * Do the repetitive munging of a list, and calculate the rolling
     * average.
     *
     * @param list List of values.
     * @param reading The raw reading to add.
     * @param bounds Any upper limit when calculating tolerances.
     * @param accuracy 10^n, where n == number of dp.
     * @return The mean, updated, value.
     */
    function _updateList(list, reading, bounds, accuracy) {
        if (list.length >= samples)
            list.pop()

        list.unshift(reading)

        var count = 0   // How many items we're counting
        var mean  = 0.0 // The mean value
        for (var i = 0; i < list.length; i++) {
            if (Math.abs((reading - list[i]) / bounds) < 0.25) {
                mean += list[i]
                count++
            }
        }

        return Math.round(accuracy * mean / count) / accuracy;
    }
//        var set   = []       // Can't update a list property, but can re-assign it
//        var mean  = rawAngle // Total of values contributing to the next
//        var count = 1        // How many items we're counting
//        for (var i = 1; i < previousAngle.length; i++) {
//            if (Math.abs((rawAngle - previousAngle[i]) / 360) < 0.25) {
//                mean += previousAngle[i]
//                count++
//            }
//            set[i - 1] = previousAngle[i]
//        }
//        set[previousAngle.length - 1] = rawAngle
//        previousAngle = set
//        var newAngle = Math.round(100 * mean / count) / 100 - calibration[1]

//        set   = []
//        mean  = rawDip
//        count = 1
//        for (var i = 1; i < previousDip.length; i++) {
//            if (Math.abs(rawDip - previousDip[i]) < 0.25) {
//                mean += previousDip[i]
//                count++
//            }
//            set[i - 1] = previousDip[i]
//        }
//        set[previousDip.length - 1] = rawDip
//        previousDip = set
//        var newDip = Math.round(1000 * mean / count) / 1000 - calibration[0]
}
