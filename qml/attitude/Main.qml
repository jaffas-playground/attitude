import QtQuick 1.0

Item {
    id: window
    width: 854
    height: 480

    /**
     * Contains the ground and the sky, and the pivot around which
     * we rotate.
     */
    Item {
        id: world
        x: -parent.width
        y: -parent.height * 2.5
        width: parent.width * 3
        height: parent.height * 6
        transformOrigin: Item.Center
        rotation: 0

        Item {
            id: view
            x: 0
            y: 0
            width: world.width
            height: world.height
            Rectangle {
                id: sky
                anchors.top: view.top
                anchors.bottom: view.verticalCenter
                anchors.left: view.left
                anchors.right: view.right
                gradient: Gradient {
                    GradientStop { position: 0; color: "#006bd6" }
                    GradientStop { position: 0.66; color: "#1e94ff" }
                    GradientStop { position: 1; color: "#1e94ff" }
                }
            }

            Rectangle {
                id: ground
                anchors.top: view.verticalCenter
                anchors.bottom: view.bottom
                anchors.left: view.left
                anchors.right: view.right
                gradient: Gradient {
                    GradientStop { position: 0; color: "#57990a" }
                    GradientStop { position: 0.66; color: "#007b00" }
                    GradientStop { position: 1; color: "#007b00" }
                }
            }
        }

        Item {
            id: horizon
            opacity: 0.8
            Rectangle {
                x: (world.width / 2) - 90
                y: world.height / 2
                width: 50
                height: 2
                color: "#ffffff"
            }
            Rectangle {
                x: (world.width / 2) + 40
                y: world.height / 2
                width: 50
                height: 2
                color: "#ffffff"
            }
        }

        Item {
            id: heightBars
            opacity: 0.8

            property int barHeight: world.height / 10
            property int barWidth: 30

            Rectangle {
                id: heightBarTop
                x: (world.width / 2) - parent.barWidth
                y: (world.height / 2) - (parent.barHeight / 2)
                width: parent.barWidth
                height: 2
                color: "#ffffff"
            }
            Rectangle {
                id: heightBarMain
                x: world.width / 2
                y: heightBarTop.y
                width: 2
                height: parent.barHeight
                color: "#ffffff"
            }
            Rectangle {
                id: heightBarBottom
                x: heightBarMain.x
                y: heightBarMain.y + parent.barHeight
                width: parent.barWidth
                height: 2
                color: "#ffffff"
            }
            Repeater {
                model: 9
                Rectangle {

                    property int _divisor : index === 0 || index === 8 ? 2
                                          :                index === 4 ? 1
                                          :                              4

                    x: heightBarMain.x - parent.barWidth / _divisor
                    y: heightBarMain.y + parent.barHeight * (index + 1)/10
                    width: parent.barWidth / (_divisor / 2)
                    height: 2
                    color: "#ffffff"
                }
            }
        }
    }

    Image {
        source: "moire.png"
        width: parent.width
        height: parent.height
    }


    /**
     * The angle meter in the top-right.
     */
    Text {
        id: telemetry
        text: "-"
        font.pointSize: 10 + window.height / 48
        x: parent.width - 35
        y: 10
        color: "#ffffff"
        onTextChanged: {
            telemetry.x = parent.width - 30 - telemetry.width
        }

        MouseArea {
            anchors.fill: parent
            onClicked: accelerometer.calibrate()
        }
    }


    /**
     * The accelerometer control logic.
     */
    CalibratedAccelerometer {
        id: accelerometer

        onMovement: {
            telemetry.text = Math.round(angle) + "\u00b0"
            view.y = -dip * world.height / 3.2
            world.rotation = angle
            heightBars.y = dip * heightBars.barHeight / 4
        }
    }
}
