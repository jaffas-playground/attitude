#ifndef _ACTIVEMONITOR_H
#define _ACTIVEMONITOR_H

#include <QObject>

/**
 * Simple QObject with a single NOTIFYable, read-only property
 * that can be used inside QML to read application active status.
 *
 * The slot activeChanged(bool) should be connected to the
 * activeChanged(bool) signal of the modified QmlApplicationViewer.
 **/

class ActiveMonitor : public QObject
{
    Q_OBJECT

private:
    bool _active;
    bool _external;

public:
    ActiveMonitor(QObject *parent) : QObject(parent), _active(true) {}
    bool active() { return _active; }
    void setActive(bool active) {
        if (_active != active) {
            _active = active;
            emit changed();
        }
    }

    bool external() { return _external; }
    void setExternal(bool external) {
        _external = external;
    }

    Q_PROPERTY(bool active READ active WRITE setActive NOTIFY changed)
    Q_PROPERTY(bool external READ external WRITE setExternal)

public slots:
    void activeChanged(bool active) {
        if (!_external)
            setActive(active);
    }

signals:
    void changed();
};

#endif
