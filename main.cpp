#include <QtGui/QApplication>
#include <QtDeclarative>
#include "qmlapplicationviewer.h"
#include "activemonitor.h"
#include <stdio.h>
#include <QDebug>


void myMessageOutput(QtMsgType type, const char* msg){
               fprintf(stdout, "%s\n", msg);
               fflush(stdout);
}


int main(int argc, char *argv[])
{
#if defined(Q_WS_MAEMO_5)
    bool portrait = false;
    bool fullscreen = false;
#elif defined(Q_OS_SYMBIAN)
    bool portrait = true;
    bool fullscreen = true;
#else
    bool portrait = true;
    bool fullscreen = true;
#endif

    QApplication app(argc, argv);
    qInstallMsgHandler(myMessageOutput);
    app.setProperty("NoMStyle", true);

    QmlApplicationViewer viewer;
    viewer.engine()->addImportPath("/opt/qtm11/imports");

    /**
     * Qt Quick 1.0 does not have the possibility to tell the QML if
     * the application is in the foreground or not. To fix this, we
     * monitor the changes manually in the viewer and expose a new
     * ActiveMonitor object that has an "active" property to the QML
     * context. Inside QML, use "activeMonitor.active" as boolean.
     **/
    ActiveMonitor *monitor = new ActiveMonitor(&viewer);
    QObject::connect(&viewer, SIGNAL(activeChanged(bool)),
                     monitor, SLOT(activeChanged(bool)));
    viewer.rootContext()->setContextProperty("activeMonitor", monitor);

    viewer.engine()->rootContext()->setContextProperty("portraitDevice", portrait);
    viewer.setSource(QUrl("qrc:///assets/qml/attitude/qtc-root.qml"));
    if (viewer.status() != QDeclarativeView::Ready || viewer.errors().length() != 0)
        viewer.setSource(QUrl("qrc:///assets/qml/attitude/Main.qml"));

    if (portrait)
        viewer.setOrientation(QmlApplicationViewer::ScreenOrientationLockLandscape);

    if (fullscreen) {
        viewer.showFullScreen();
    } else {
        viewer.show();
    }

    app.exec();
}
