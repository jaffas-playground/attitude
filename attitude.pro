# Add more folders to ship with the application, here
folder_01.source = qml/attitude
folder_01.target = qml
DEPLOYMENTFOLDERS = folder_01

APP_NAME=Attitude
TARGET=Attitude
VERSION=0.9.5

# Additional import path used to resolve QML modules in Creator's code model
QML_IMPORT_PATH =

symbian:TARGET.UID3 = 0xE6DA552B

symbian:TARGET.CAPABILITY = NetworkServices

# Define QMLJSDEBUGGER to allow debugging of QML in debug builds
# (This might significantly increase build time)
# DEFINES += QMLJSDEBUGGER

CONFIG += mobility
MOBILITY += sensors

device|simulator {
  CONFIG += qt warn_on debug_and_release cascades
}

# The .cpp file which was generated for your project. Feel free to hack it.
SOURCES += main.cpp

# Please do not modify the following two lines. Required for deployment.
include(qmlapplicationviewer/qmlapplicationviewer.pri)
qtcAddDeployment()

HEADERS += activemonitor.h

OTHER_FILES += \
    qtc_packaging/debian_harmattan/rules \
    attitude.desktop \
    qtc_packaging/debian_fremantle/changelog \
    qtc_packaging/debian_fremantle/compat \
    qtc_packaging/debian_fremantle/control \
    qtc_packaging/debian_fremantle/copyright \
    qtc_packaging/debian_fremantle/README \
    qtc_packaging/debian_fremantle/rules \
    qtc_packaging/debian_harmattan/control

unix:!symbian:!maemo5 {
    desktopfile.files = $${TARGET}.desktop
    desktopfile.path = /usr/share/applications
    INSTALLS += desktopfile
}

RESOURCES += \
    assets.qrc
